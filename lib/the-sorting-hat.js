'use strict';
const HatAdapter = require('./adapters/hat-adapter');

/**
 * @classdesc
 *
 * This is The Sorting Hat class
 * It's the entry point for group- and permission handling
 *
 * @param {HatAdapter} hatAdapter - an instance of HatAdapter is required.
 * @throws {TypeError} When given argument is not an instance of HatAdapter
 * @class
 */
function TheSortingHat(hatAdapter) {

    if (!(hatAdapter instanceof HatAdapter)) {
        throw new TypeError('adapter and/or processor not of expected type');
    }
    const adapter = hatAdapter;


    Object.defineProperty(this, 'adapter', {
        get: function () {
            return adapter;
        }
    });
    Object.defineProperty(this, 'config', {
        writable: true,
        enumerable: true,
        configurable: true
    });
}

/**
 * Initializes The Sorting Hat
 * (it initializes the given adapter)
 *
 * @param {function} callback
 */
TheSortingHat.prototype.initialize = function (callback) {
    this.adapter.initialize(callback);
};

/**
 * Finds a user
 * (Is delegated to a given adapter)
 *
 * @param {object} criteria - User to find
 * @param {function} callback
 */
TheSortingHat.prototype.findUser = function (criteria, callback) {
    this.adapter.findUser(criteria, callback);
};

/**
 * Finds a group
 * (Is delegated to a given adapter)
 *
 * @param {object} criteria - Group to find
 * @param {function} callback
 */
TheSortingHat.prototype.findGroup = function (criteria, callback) {
    this.adapter.findGroup(criteria, callback);
};


/**
 * Finds a permission
 * (Is delegated to a given adapter)
 *
 * @param {object} criteria - Permission to find
 * @param {function} callback
 */
TheSortingHat.prototype.findPermission = function (criteria, callback) {
    this.adapter.findPermission(criteria, callback);
};

/**
 * Adds a user
 * (Is delegated to a given adapter)
 *
 * @param {object} user - User to add
 * @param {function} callback
 */
TheSortingHat.prototype.addUser = function (user, callback) {
    this.adapter.addUser(user, callback);
};

/**
 * Add a group
 * (Is delegated to a given adapter)
 *
 * @param {object} group - Group to add
 * @param {function} callback
 */
TheSortingHat.prototype.addGroup = function (group, callback) {
    this.adapter.addGroup(group, callback);
};

/**
 * Adds an array of groups
 * (Is delegated to a given adapter)
 *
 * @param {Array<object>} groups - Collection of groups to add
 * @param {function} callback
 */
TheSortingHat.prototype.addGroups = function (groups, callback) {
    this.adapter.addGroups(groups, callback);
};

/**
 * Add a permission
 * (Is delegated to a given adapter)
 *
 * @param {object} permission - Permission to add
 * @param {function} callback
 */
TheSortingHat.prototype.addPermission = function (permission, callback) {
    this.adapter.addPermission(permission, callback);
};

/**
 * Adds an array of permissions to the initialized adapter
 * (Is delegated to a given adapter)
 *
 * @param {Array<object>} permissions - Collection of permissions to add
 * @param {function} callback
 */
TheSortingHat.prototype.addPermissions = function (permissions, callback) {
    this.adapter.addPermissions(permissions, callback);
};

/**
 * Removes a user
 * (Is delegated to a given adapter)
 *
 * @param {object} user - User criteria for removal of a certain user
 * @param {function} callback
 */
TheSortingHat.prototype.removeUser = function (user, callback) {
    this.adapter.removeUser(user, callback);
};

/**
 * Removes a group
 * (Is delegated to a given adapter)
 *
 * @param {object} group - Group criteria for removal of a certain group
 * @param {function} callback
 */
TheSortingHat.prototype.removeGroup = function (group, callback) {
    this.adapter.removeGroup(group, callback);
};

/**
 * Removes a permission
 * (Is delegated to a given adapter)
 *
 * @param {object} permission - Permission criteria for removal of a certain permission
 * @param {function} callback
 */
TheSortingHat.prototype.removePermission = function (permission, callback) {
    this.adapter.removePermission(permission, callback);
};

/**
 * Updates an existing user, and if not found inserts the user
 * (Is delegated to a given adapter)
 *
 * @param {object} criteria - User criteria to update when found
 * @param {object} user - Data to merge with existing user, or to insert
 * @param {function} callback
 */
TheSortingHat.prototype.upsertUser = function (criteria, user, callback) {
    this.adapter.upsertUser(criteria, user, callback);
};


/**
 * Updates an existing group, and if not found inserts the group
 * (Is delegated to a given adapter)
 *
 * @param {object} criteria - Group criteria to update when found
 * @param {object} group - Data to merge with existing group, or to insert
 * @param {function} callback
 */
TheSortingHat.prototype.upsertGroup = function (criteria, group, callback) {
    this.adapter.upsertGroup(criteria, group, callback);
};

/**
 * Updates an existing permission, and if not found inserts the permission
 * (Is delegated to a given adapter)
 *
 * @param {object} criteria - Permission criteria to update when found
 * @param {object} permission - Data to merge with existing permission, or to insert
 * @param {function} callback
 */
TheSortingHat.prototype.upsertPermission = function (criteria, permission, callback) {
    this.adapter.upsertPermission(criteria, permission, callback);
};

/**
 * Finds a group collection attached to this certain user
 * (Is delegated to a given adapter)
 *
 * @param {object} user - User criteria to find the corresponding groups
 * @param {function} callback
 */
TheSortingHat.prototype.findGroupsByUser = function (user, callback) {
    this.adapter.findGroupsByUser(user, callback);
};

/**
 * Finds a permission collection attached to this certain user
 * (Is delegated to a given adapter)
 *
 * @param {object} user - User criteria to find the corresponding permissions
 * @param {function} callback
 */
TheSortingHat.prototype.findPermissionsByUser = function (user, callback) {
    this.adapter.findPermissionsByUser(user, callback);
};

/**
 * Validates given user against given array of permission positions.
 * <b>All</b> given permissions need to be found at this given user
 * (Is delegated to a given adapter)
 *
 * @param {object} user - User as criteria to validate
 * @param {Array<Number>} permissions
 * @param {function} callback
 */
TheSortingHat.prototype.validateUserWithAllPermissions = function (user, permissions, callback) {
    this.adapter.validateUserWithAllPermissions(user, permissions, callback);
};

/**
 * Validates given user against given array of permission positions.
 * <b>If any</b> given permission is found attached to this given user
 * (Is delegated to a given adapter)
 *
 * @param {object} user - User to validate permissions against
 * @param {Array<Number>} permissions - Permission collection of integers
 * @param {function} callback
 */
TheSortingHat.prototype.validateUserWithAnyPermissions = function (user, permissions, callback) {
    this.adapter.validateUserWithAnyPermissions(user, permissions, callback);
};


module.exports = TheSortingHat;
