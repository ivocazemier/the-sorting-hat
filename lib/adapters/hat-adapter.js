'use strict';
const util = require('util');
const EventEmitter = require('events').EventEmitter;


const INHERITANCE_ERROR = 'Abstract method, override please.';
/**
 * @classdesc
 *
 * This class should be inherited and acts as contract (Interface)
 *
 * @extends EventEmitter
 * @class
 * @abstract
 */
function HatAdapter() {
    EventEmitter.call(this);
}

util.inherits(HatAdapter, EventEmitter);


/**
 * Initialize adapter
 *
 * @abstract
 */
HatAdapter.prototype.initialize = function () {
    throw new Error(INHERITANCE_ERROR);
};


HatAdapter.prototype.addUser = function () {
    throw new Error(INHERITANCE_ERROR);
};

/**
 * Add user group
 *
 * @abstract
 */
HatAdapter.prototype.addGroup = function () {
    throw new Error(INHERITANCE_ERROR);
};

/**
 * Add array of user groups
 *
 * @abstract
 */
HatAdapter.prototype.addGroups = function () {
    throw new Error(INHERITANCE_ERROR);
};

/**
 * Add group permission
 * @abstract
 */
HatAdapter.prototype.addPermission = function () {
    throw new Error(INHERITANCE_ERROR);
};

/**
 * Remove a user
 * @abstract
 */
HatAdapter.prototype.removeUser = function () {
    throw new Error(INHERITANCE_ERROR);
};

/**
 * Remove a group
 * @abstract
 */
HatAdapter.prototype.removeGroup = function () {
    throw new Error(INHERITANCE_ERROR);
};

/**
 * Remove a permission
 * @abstract
 */
HatAdapter.prototype.removePermission = function () {
    throw new Error(INHERITANCE_ERROR);
};


/**
 * Update or insert user (if not exists)
 * @abstract
 */
HatAdapter.prototype.upsertUser = function () {
    throw new Error(INHERITANCE_ERROR);
};

/**
 * Update or insert group (if not exists)
 * @abstract
 */
HatAdapter.prototype.upsertGroup = function () {
    throw new Error(INHERITANCE_ERROR);
};

/**
 * Update or insert permission (if not exists)
 * @abstract
 */
HatAdapter.prototype.upsertPermission = function () {
    throw new Error(INHERITANCE_ERROR);
};

/**
 * Find user groups
 * @abstract
 */
HatAdapter.prototype.findGroupsByUser = function () {
    throw new Error(INHERITANCE_ERROR);
};

/**
 * Find user permissions
 * @abstract
 */
HatAdapter.prototype.findPermissionsByUser = function () {
    throw new Error(INHERITANCE_ERROR);
};


/**
 * Find user
 * @abstract
 */
HatAdapter.prototype.findUser = function () {
    throw new Error(INHERITANCE_ERROR);
};

/**
 * Find group
 * @abstract
 */
HatAdapter.prototype.findGroup = function () {
    throw new Error(INHERITANCE_ERROR);
};

/**
 * Find permission
 * @abstract
 */
HatAdapter.prototype.findPermission = function () {
    throw new Error(INHERITANCE_ERROR);
};


/**
 * Validate a user against e.g. given permissions <br>
 * All permissions given, need to correspond with the permissions owned by this user.
 *
 * @abstract
 */
HatAdapter.prototype.validateUserWithAllPermissions = function () {
    throw new Error(INHERITANCE_ERROR);
};

/**
 * Validate a user against e.g. given permissions <br>
 * When one of the given permissions are found at this user, user is valid.
 * @abstract
 */
HatAdapter.prototype.validateUserWithAnyPermissions = function () {
    throw new Error(INHERITANCE_ERROR);
};

module.exports = HatAdapter;
