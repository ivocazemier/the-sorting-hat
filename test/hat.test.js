'use strict';
const expect = require('chai').expect;
const helper = require('./helper');
const Hat = require('../index').Hat;


describe('Hat: Bit masking tests', function () {

    describe('Happy flows', function () {

        describe('Compare hats', function () {

            it(`Test ${helper.testNumber++}: Compare reference / alias)`, function (done) {

                // Initialize a Hat with ArrayBuffer (size = 1 bytes)
                const hat = new Hat.create(1);
                const hatAlias = hat;

                const same = hat.compare(hatAlias);
                expect(same).to.be.equal(true);
                done();

            });

            it(`Test ${helper.testNumber++}: Compares different size but same data)`, function (done) {

                // Initialize a Hat with ArrayBuffer (size = 1 bytes)
                const hat1 = Hat.create(1);
                const hat2 = Hat.create(2);

                hat1.setPosition(3).setPosition(6);
                hat2.setPosition(3).setPosition(6);

                let same = hat1.compare(hat2);
                expect(same).to.be.equal(true);

                same = hat2.compare(hat1);
                expect(same).to.be.equal(true);

                done();

            });

            it(`Test ${helper.testNumber++}: Compares different size and different data)`, function (done) {

                // Initialize a Hat with ArrayBuffer (size = 1 bytes)

                const hat1 = Hat.create(1);
                const hat2 = Hat.create(2);
                hat2.setPosition(16);

                let same = hat1.compare(hat2);
                expect(same).to.be.equal(false);

                same = hat2.compare(hat1);
                expect(same).to.be.equal(false);

                done();

            });

            it(`Test ${helper.testNumber++}: Compare replicated data)`, function (done) {

                // Initialize a Hat with ArrayBuffer (size = 256 bytes)
                const hat1 = Hat.create(256);

                // Pre set some bit positions for the test:
                hat1.setPosition(1)
                    .setPosition(2)
                    .setPosition(3)
                    .setPosition(4)
                    .setPosition(5)
                    .setPosition(6)
                    .setPosition(7)
                    .setPosition(8)
                    .setPosition(9)
                    .setPosition(10)
                    .setPosition(11)
                    .setPosition(12)
                    .setPosition(13);

                const str1 = hat1.toString();
                //////////////////////////////////


                //////////////////////////////////
                // Convert string to a new Hat instance and check if bit positions still comply
                const hat2 = Hat.fromString(str1);


                // Assertions:
                expect(hat1.compare(hat2)).to.be.equal(true);
                expect(hat2.compare(hat1)).to.be.equal(true);

                done();

            });
        });

        describe('From to String', function () {


            it(`Test ${helper.testNumber++}: Set bits (Converts from string and back and checks consistency)`, function (done) {

                // Initialize a Hat with ArrayBuffer (size = 256 bytes)
                const hat = Hat.create(256);

                // Pre set some bit positions for the test:
                hat.setPosition(1)
                    .setPosition(2)
                    .setPosition(3)
                    .setPosition(4)
                    .setPosition(5)
                    .setPosition(6)
                    .setPosition(7)
                    .setPosition(8)
                    .setPosition(9)
                    .setPosition(10)
                    .setPosition(11)
                    .setPosition(12)
                    .setPosition(13);

                // We're going to check on these positions
                const positions = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13];

                // Check before encoding to string
                const matches1 = hat.hasAllFromPositions(positions);
                const str1 = hat.toString();
                //////////////////////////////////


                //////////////////////////////////
                // Convert string to a new Hat instance and check if bit positions still comply
                const hat1 = Hat.fromString(str1);
                const matches2 = hat1.hasAllFromPositions(positions);

                // Assertions:
                expect(matches1).to.be.equal(true);
                expect(matches2).to.be.equal(true);
                done();

            });


            it(`Test ${helper.testNumber++}: Toggle bits (Converts from string and back and checks consistency)`, function (done) {

                // Initialize a Hat with ArrayBuffer (size = 256 bytes)
                const hat = Hat.create(256);

                // Pre set some bit positions for the test:
                hat.setPosition(1)
                    .setPosition(2)
                    .setPosition(3)
                    .setPosition(4)
                    .setPosition(5)
                    .setPosition(6)
                    .setPosition(7)
                    .setPosition(8)
                    .setPosition(9)
                    .setPosition(10)
                    .setPosition(11)
                    .setPosition(12)
                    .setPosition(13);

                // Toggle some
                hat.togglePosition(1)
                    .togglePosition(3)
                    .togglePosition(5)
                    .togglePosition(7)
                    .togglePosition(9)
                    .togglePosition(11)
                    .togglePosition(13);

                // We're going to check on these positions
                const positions = [-1, 2, -3, 4, -5, 6, -7, 8, -9, 10, -11, 12, -13];

                // Check before encoding to string
                const matches1 = hat.hasAllFromPositions(positions);
                const str1 = hat.toString();
                //////////////////////////////////


                //////////////////////////////////
                // Convert string to a new Hat instance and check if bit positions still comply
                const hat1 = Hat.fromString(str1);
                const matches2 = hat1.hasAllFromPositions(positions);

                // Assertions:
                expect(matches1).to.be.equal(true);
                expect(matches2).to.be.equal(true);
                done();

            });


            it(`Test ${helper.testNumber++}: Individual bit positions (Sets some individual bits and checks outcome)`, function (done) {

                // Initialize a Hat with ArrayBuffer (size = 2 bytes)
                const hat = Hat.create(2);

                // Pre set some bit positions for the test:
                hat.setPosition(1)
                    .setPosition(2)
                    .setPosition(3)
                    .setPosition(4)
                    .setPosition(5)
                    .setPosition(6)
                    .setPosition(7)
                    .setPosition(8)
                    .setPosition(9)
                    .setPosition(10)
                    .setPosition(11)
                    .setPosition(12)
                    .setPosition(13);

                const str1 = hat.toString();
                //////////////////////////////////


                //////////////////////////////////
                // Convert string to a new Hat instance
                const hat1 = Hat.fromString(str1);

                // Assertions:
                expect(hat1.isPosition(1)).to.be.equal(true);
                expect(hat1.isPosition(2)).to.be.equal(true);
                expect(hat1.isPosition(3)).to.be.equal(true);
                expect(hat1.isPosition(4)).to.be.equal(true);
                expect(hat1.isPosition(5)).to.be.equal(true);
                expect(hat1.isPosition(6)).to.be.equal(true);
                expect(hat1.isPosition(7)).to.be.equal(true);
                expect(hat1.isPosition(8)).to.be.equal(true);

                expect(hat1.isPosition(9)).to.be.equal(true);
                expect(hat1.isPosition(10)).to.be.equal(true);
                expect(hat1.isPosition(11)).to.be.equal(true);
                expect(hat1.isPosition(12)).to.be.equal(true);
                expect(hat1.isPosition(13)).to.be.equal(true);
                expect(hat1.isPosition(14)).to.be.equal(false);
                expect(hat1.isPosition(15)).to.be.equal(false);
                expect(hat1.isPosition(16)).to.be.equal(false);


                done();

            });

            it(`Test ${helper.testNumber++}: Individual bit positions (Sets some individual bits, manipulate and checks outcome)`, function (done) {

                // Initialize a Hat with ArrayBuffer (size = 2 bytes)
                const hat = Hat.create(2);

                // Pre set some bit positions for the test:
                hat.setPosition(1)
                    .setPosition(2)
                    .setPosition(3)
                    .setPosition(4)
                    .setPosition(5)
                    .setPosition(6)
                    .setPosition(7)
                    .setPosition(8);


                expect(hat.isPosition(1)).to.be.equal(true);
                expect(hat.isPosition(2)).to.be.equal(true);
                expect(hat.isPosition(3)).to.be.equal(true);
                expect(hat.isPosition(4)).to.be.equal(true);
                expect(hat.isPosition(5)).to.be.equal(true);
                expect(hat.isPosition(6)).to.be.equal(true);
                expect(hat.isPosition(7)).to.be.equal(true);
                expect(hat.isPosition(8)).to.be.equal(true);

                hat.changePosition(1, false)
                    .changePosition(3, false)
                    .changePosition(5, false)
                    .changePosition(7, false)
                    .changePosition(8, true);

                expect(hat.isPosition(1)).to.be.equal(false);
                expect(hat.isPosition(2)).to.be.equal(true);
                expect(hat.isPosition(3)).to.be.equal(false);
                expect(hat.isPosition(4)).to.be.equal(true);
                expect(hat.isPosition(5)).to.be.equal(false);
                expect(hat.isPosition(6)).to.be.equal(true);
                expect(hat.isPosition(7)).to.be.equal(false);
                expect(hat.isPosition(8)).to.be.equal(true);

                hat.clearPosition(2)
                    .clearPosition(4)
                    .clearPosition(6)
                    .clearPosition(8);

                const positions = [-1, -2, -3, -4, -5, -6, -7, -8];

                expect(hat.hasAllFromPositions(positions)).to.be.equal(true);

                done();

            });
        });

        describe('Get / Set positions array', function () {

            it(`Test ${helper.testNumber++}: Fetch positions where bit is set true`, function (done) {

                // Initialize a Hat with ArrayBuffer (size = 2 bytes)
                const hat = Hat.create(2);

                // Pre set some bit positions for the test:
                hat.setPosition(1)
                    .setPosition(2)
                    .setPosition(3)
                    .setPosition(4)
                    .setPosition(5)
                    .setPosition(6)
                    .setPosition(7)
                    .setPosition(8)
                    .setPosition(10);

                const positions = hat.getPositionsArray();
                const match = hat.hasAllFromPositions(positions);

                expect(positions).to.be.array;
                expect(match).to.be.true;
                done();
            });

            it(`Test ${helper.testNumber++}: Parse positions`, function (done) {

                // Initialize a Hat with ArrayBuffer (size = 2 bytes)
                const hat = Hat.create(2);

                const ref = [1, 2, -3, 4, 5, 6, 7, 8, -10];
                // Pre set some bit positions for the test:
                hat.setAllFromPositions(ref);
                const positions = hat.getPositionsArray();
                const match = hat.hasAllFromPositions(ref);

                expect(positions).to.be.array;
                expect(match).to.be.true;
                done();
            });

        });

    });

    describe('Unhappy flows ', function () {


        it(`Test ${helper.testNumber++}: Bit position \'0\' is not allowed (positions are allowed from 1 and up)`, function (done) {

            var throwError = function () {
                // Initialize a Hat with ArrayBuffer (size = 256 bytes)
                const hat = Hat.create(256);

                // Pre set bit position 0 for the test:
                hat.setPosition(0);

            };

            expect(throwError).to.throw(Error);
            done();
        });

        it(`Test ${helper.testNumber++}: Compare hat with hat please)`, function (done) {

            function throwError() {
                // Initialize a Hat with ArrayBuffer (size = 256 bytes)
                const hat = Hat.create(256);

                hat.compare('WEIRD');
            }

            expect(throwError).to.throw(TypeError);
            done();
        });

        it(`Test ${helper.testNumber++}: Class method expects string`, function (done) {

            function throwError() {
                Hat.fromString(556);
            }

            expect(throwError).to.throw(TypeError);
            done();
        });


        it(`Test ${helper.testNumber++}: Bit position \'9\' is not allowed anymore (we only allocated 1 byte)`, function (done) {

            function throwError() {
                // Initialize a Hat with ArrayBuffer (size = 1 bytes)
                const hat = Hat.create(1);

                // Pre set bit position 0 for the test:
                hat.setPosition(1);
                hat.setPosition(2);
                hat.setPosition(3);
                hat.setPosition(4);
                hat.setPosition(6);
                hat.setPosition(7);
                hat.setPosition(8); // Last bit position
                hat.setPosition(9); // This position should throw an error!
                hat.setPosition(10);
            }

            expect(throwError).to.throw(Error, /Illegal position/);
            done();
        });

        it(`Test ${helper.testNumber++}: Checks on positions outside the memory area, should just return false early`, function (done) {

            // Initialize a Hat with ArrayBuffer (size = 1 bytes)
            const hat = Hat.create(1);

            // Pre set all bits positions for the test:
            hat.setPosition(1)
                .setPosition(2)
                .setPosition(3)
                .setPosition(4)
                .setPosition(5)
                .setPosition(6)
                .setPosition(7)
                .setPosition(8);

            // We're going to check on these and more, this should return `false`
            const positions = [1, 2, 3, 4, 5, 6, 7, 8, 200, 9];

            // Check before encoding to string
            const result = hat.hasAllFromPositions(positions);

            expect(result).to.be.equal(false);
            done();

        });

        it(`Test ${helper.testNumber++}: force type strictness on hasAllFromPositions`, function (done) {

            function throwTypeError() {
                // Initialize a Hat with ArrayBuffer (size = 1 bytes)
                const hat = Hat.create(1);

                // We're going to check on these and more, this should return `false`
                const positions = 'Weird';

                // Check before encoding to string
                hat.hasAllFromPositions(positions);
            }

            expect(throwTypeError).to.throw(TypeError);
            done();

        });

        it(`Test ${helper.testNumber++}: Try to do things out of bounds`, function (done) {

            // Initialize a Hat with ArrayBuffer (size = 1 bytes)
            const hat = Hat.create(1);

            const ref = [1, 2, -3, 4, 5, 6, 7, 8, -10]; // -10 is out of bounds
            // Pre set some bit positions for the test:
            hat.setAllFromPositions(ref);
            const positions = hat.getPositionsArray();
            const match = hat.hasAllFromPositions(ref);

            expect(positions).to.be.array;
            expect(match).to.be.false;
            done();
        });

        it(`Test ${helper.testNumber++}: Try to do have something other than array`, function (done) {

            // Initialize a Hat with ArrayBuffer (size = 1 bytes)
            const hat = Hat.create(1);

            function throwError() {
                hat.setAllFromPositions('w00t!');
            }

            expect(throwError).to.throw(TypeError);
            done();
        });
    });

});


