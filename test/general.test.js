'use strict';
const expect = require('chai').expect;
const helper = require('./helper');

const Hat = require('../lib/hat');
const HatAdapter = require('../lib/adapters/hat-adapter');


describe('Sorting Hat: Generic tests', function () {

    describe('Unhappy flows ', function () {

        it(`Test ${helper.testNumber++}: This should throw a TypeError because Hat expects an ArrayBuffer`, function () {

            function throwError() {
                new Hat();
            }
            expect(throwError).to.throw(TypeError, /argument not an instance of ArrayBuffer/);

        });

        it(`Test ${helper.testNumber++}: This should throw a TypeError`, function () {

            const adapter = new HatAdapter();

            function throwError() {
                adapter.initialize();
            }
            expect(throwError).to.throw(Error, /Abstract method, override please./);

        });


    });
});
