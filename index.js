'use strict';

module.exports = {
    TheSortingHat: require('./lib/the-sorting-hat'),
    HatAdapter: require('./lib/adapters/hat-adapter'),
    Hat: require('./lib/hat'),
    HatByteProcessor: require('./lib/hat-processor').HatByteProcessor,
    LokiJSHatAdapter: require('./lib/adapters/lokijs-hat-adapter')
};
