module.exports = {
    opts: {
        destination: './docs',
        tutorials: './tutorials',
        recurse: true
    },
    plugins: [
        'plugins/markdown'
    ],
    templates: {
        path: 'ink-docstrap',
        systemName: 'The Sorting Hat',
        footer: 'The Sorting Hat (I. Cazemier)',
        navType: 'inline',
        theme: 'superhero',
        linenums: true,
        collapseSymbols: false,
        inverseNav: false,
        outputSourceFiles: true,
        syntaxTheme: 'dark',
        includeDate: true
    }
};
